from flask import Flask

app = Flask(__name__)
app.config.from_object("config")

# ======================
# = Register Bluprints =
# ======================
from views.search import search
app.register_blueprint(search)
