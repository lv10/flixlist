import sys

from pymongo import MongoClient

# -------------------------------------------
#        Connnection & Setup
# -------------------------------------------

connection = MongoClient(safe=True)
db = connection.moviesDB
movies = db.movies

# -------------------------------------------
#            Pass Data to the DB
# -------------------------------------------


def insert_movie(movie):
    movies.insert(movie)


def search_title(search_title):
    """
        Returns a tuple with cursor (results) and the size of the cursor
    """
    query = {"Name": {"$regex": search_title, "$options": "i"},
             "BoxArt.LargeUrl": {"$ne": None}}
    projection = {"Name": 1, "BoxArt": 1, "Id": 1}

    try:
        results = movies.find(query, projection)
    except:
        print "Unexpected error: ", sys.exc_info()[0]

    return results, results.count()


def search_by_id(t_id):
    """
        Returns a dict with all the information related to the requested id
    """
    query = {"Id": t_id}
    projection = {"_id": 0}

    try:
        result = movies.find_one(query, projection)
    except:
        print "Unexpected error: ", sys.exc_info()[0]
    return result
