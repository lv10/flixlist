#################################################
# Movie Service API client Abstract Class
#################################################


class MovieServiceAPI(object):
    """
        Abstract class of a MovieServiceAPI
    """

    def search_by_title(self, search_term):
        raise NotImplementedError
