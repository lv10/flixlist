import re
import uuid
import json
from datetime import datetime

import oauth2
from BeautifulSoup import BeautifulSoup

from movies import MovieServiceAPI

# ===========================================================================
#                           Netflix JSON REST API
# ===========================================================================


class NetflixAuthRequiredError(Exception):
    """ Error thrown if Authorization is required """
    pass


class NetflixError(Exception):
    """ Error thrown is the API returns/throws an http error """
    pass


class NetflixRESTfulAPI(MovieServiceAPI):

    #Authorization URLS
    BASE_URL = u"http://api-public.netflix.com/"
    REQUEST_TOKEN_URL = BASE_URL + u"oauth/request_token"
    ACCESS_TOKEN_URL = BASE_URL + u"auth/access_token"
    AUTHORIZE_URL = u"https://api-user.netflix.com/oauth/login"

    def __init__(self, consumer_key, shared_secret):
        if not consumer_key:
            raise NetflixError("consumer_key must not be null")

        if not shared_secret:
            raise NetflixError("consumer_secret/shared_secret cannot be null")

        self.CONSUMER_KEY = consumer_key
        self.SHARED_SECRET = shared_secret

    # -----------------------------------------
    #              Aux functions
    # -----------------------------------------

    def _get_url_Netflix(self, query, q_type, q_id_type=None):
        """
            Returns a url based on the conditions passed to the function.
            If the q_type is "title" then it returns a url to query for
            the basic information in JSON response, like "BoxArt", "Title"
            and "Year".

            params:
              :param: str: query: id or title of the movie searched.
              :param: str: q_type: "id" or "title", used as condition
                                    to determine the url to return.

            rType: String
        """
        titles_url = self.BASE_URL + u"catalog/titles"
        the_format = u"&output=json"

        if q_type == "title":
            term = "?term=%s" % query
            max_results = "/max_results=50&"
            return titles_url + term + max_results + the_format

        if q_type == "id":
            netflix_id = query
            id_type = u"/%s/" % q_id_type
            _EXPANDS = [u"synopsis", u"cast", u"directors", u"formats",
                        u"web page", "languages and audio", "discs",
                        u"similars", u"awards"]
            extends = u"?&expand=" + ",".join(_EXPANDS)
            return titles_url + id_type + netflix_id + extends + the_format

    def get_timestamp(self):
        """
            Returns the difference in seconds between January 1st, 1970 at
            00:00:00 (HH:MM:SS) UNIX Date.

            rType: Integer

            Notes:
             - Commented out, there is an alternate version to get the
               timestamp from Netflix in case the clocks of the server
               and Netflix are not synced. The cushion time difference
               from Netflix servers is 10 mins, meaning that the result
               (Integer time difference) can be less than 600 secs off.
        """
        today = datetime.now()
        old = datetime(1970, 1, 1)
        dif = abs(today - old)
        return (dif.days * (24 * 60 * 60)) + dif.seconds

    def create_signed_request(self, query_url):
        """
           Returns a tuple with the response from Netflix. The response is
           composed by headers and content.
           params:
             :param: str: query_url: url generated by _get_url_Netflix,
                                     the url is the final query data to
                                     be requested from the API.
            rType: Tuple (dict, json)
        """

        # Create a consumer and client objects
        consumer = oauth2.Consumer(key=self.CONSUMER_KEY,
                                   secret=self.SHARED_SECRET)
        client = oauth2.Client(consumer)

        # Define parameters to be sent in the signed request as per
        # Netflix OAuth requirments for Signed Requests.
        params = {
            "oauth_version": "1.0",
            "oauth_nonce": str(uuid.uuid4()),
            "oauth_timestamp": self.get_timestamp(),
            "oauth_consumer_key": consumer.key
        }

        # Create a request
        req = oauth2.Request(method="GET", url=query_url, parameters=params)

        # Define Signature method
        signature_method = oauth2.SignatureMethod_HMAC_SHA1()

        # Sign the request
        req.sign_request(signature_method, consumer, token=None)

        # Issue complete signed request
        resp, content = client.request(query_url, "GET")

        if resp["status"] == "200":
            return content
        else:
            return "There an error. Error Code %s" % resp["status"]

    def get_available_formats(self, link_list):

        """
            Takes the "link" element which is a list from the json returned by
            the "search_by_id". This function gets the formats in which the
            record is available. Returns a list with the available formats.
            :params:
             :param: list: list of links
            rType: list
        """
        formats = filter(lambda x: x["title"] == "formats", link_list)
        availability = formats[0]["delivery_formats"]["availability"]

        if isinstance(availability, dict):
            return availability["category"]["term"]
        else:
            return [format["category"]["term"] for format in availability]

    def get_awards(self, link_list):
        """
            Takes the "link" element which is a list from the json returned
            by the "search_by_id". This function gets the awards the record
            has won. Returns a list with the awards won for a title.
            :params:
             :param: list: list of links
            rType: list
        """
        awards = filter(lambda x: x["title"] == "awards", link_list)

        if "award_winner" in awards[0]["awards"]:
            awards_won = awards[0]["awards"]["award_winner"]
            if isinstance(awards_won, list):
                return [award["category"]["term"] for award in awards_won]
            else:
                return [awards_won["category"]["term"]]

    def get_cast(self, link_list):
        """
            Takes the "link" element which is a list from the json returned
            by the "search_by_id". This function gets the cast members of a
            particular record. Returns a list cast members for a title.
            rType: list
        """
        cast = filter(lambda x: x["title"] == "cast", link_list)[0]

        if "people" not in cast:
            return None
        elif isinstance(cast["people"]["link"], dict):
            return [cast["people"]["link"]["title"]]
        else:
            people = cast["people"]["link"]
            return [actor["title"] for actor in people]

    def get_directors(self, link_list):
        """
            Takes the "link" element which is a list from the json returned
            by the "search_by_id". This function gets the director of a
            particular record. Returns the name of a movie/series director.
            rType: unicode string
        """
        directors = filter(lambda x: x["title"] == "directors", link_list)[0]
        if isinstance(directors, list):
            return [director["title"]
                    for director in directors["people"]["link"]["title"]]
        else:
            try:
                director = [directors["people"]["link"]["title"]]
            except KeyError:
                director = None

            return director

    def get_web_pages(self, link_list):
        """
            Takes the "link" element which is a list from the json returned
            by the "search_by_id". This function gets the available links such
            as official website and netflix"s link for a particular record.
            Returns a list of dicts: [{"link_type": url}, {"link_type": url}].
            rType: list of dicts
            rFormat: [{"link_type": url}, {"link_type": url}]
        """
        out = []

        netflix_site = filter(lambda x: x["title"] == "web page", link_list)
        netflix_site = {"Netflix": netflix_site[0]["href"]}
        official_site = filter(lambda x: x["title"] == "official webpage",
                               link_list)

        if len(official_site) > 0:
            out.append(netflix_site)
            out.append({"Official": official_site[0]["href"]})
        else:
            out.append(netflix_site)
        return out

    def get_similars(self, link_list):
        """
            Takes the "link" element which is a list from the json returned
            by the "search_by_id". This function gets the first five records
            in the similars category. Returns a list of records titles.
            rType: list of strings
        """
        similar = filter(lambda x: x["title"] == "similars", link_list)
        titles = similar[0]["catalog_titles"]["link"]
        return [{titles[i]["title"]: titles[i]["href"]} for i in range(5)]

    def get_synopsis(self, link_list):
        """
            Takes the "link" element which is a list from the json returned
            by the "search_by_id". This function gets the synopsis from a
            particular record. Some times the synopsis comes with html <a href
            tags to link to the cast or other movies, so an internal function
            called _convert_to_text was created. Returns the synopsis of
            particular record.

            functions:
                - _convert_to_text(txt) takes a "txt" string parameter returned
                  after filtering-out the synopsis. Using regex the "<" ">".
                  Show where the html might be. Then using BeatifulSoup, the
                  "<a></a>" are found, its content is retrieved and replaced in
                  the original "txt". The recursively (tail recursive) the
                  newly replaced "txt" is passed back into _convert_to_text().

            rType: string
        """
        synopsis = filter(lambda x: x["title"] == "synopsis",
                          link_list)[0]["synopsis"]

        def _convert_to_text(txt):
            while BeautifulSoup(txt).a:
                r = BeautifulSoup(txt)
                no_html = str(r.a.string)
                html = str(r.a)
                txt = txt.replace(html, no_html)
                _convert_to_text(txt)
            return txt
        return _convert_to_text(synopsis)

    # -----------------------------------------
    #              Search functions
    # -----------------------------------------

    def search_by_title(self, search_term):
        """
            Returns a tuple with a list of dicts (movies) and its length
        """
        query_url = self._get_url_Netflix(search_term, "title")
        req = self.create_signed_request(query_url)
        json_doc = json.loads(req)
        results = json_doc["catalog_titles"]["catalog_title"]

        for item in results:
            id_url = item["id"]
            id_data = re.search(r"\/(\w+)\/(\d+)", id_url)

            id_type = id_data.group(1)
            id_code = id_data.group(2)

            item[u"id_type"] = u"".join(id_type)
            item["id"] = id_code

        return results, len(results)

    def search_by_id(self, n_id_type, n_id):
        """
            Returns a dict with the data for the movie under Netflix"s Id
        """
        query_url = self._get_url_Netflix(n_id, "id", n_id_type)
        req = self.create_signed_request(query_url)

        j_doc = json.loads(req)["catalog_title"]
        j_doc["synopsis"] = self.get_synopsis(j_doc["link"])
        j_doc["format"] = self.get_available_formats(j_doc["link"])
        j_doc["awards"] = self.get_awards(j_doc["link"])
        j_doc["cast"] = self.get_cast(j_doc["link"])
        j_doc["directors"] = self.get_directors(j_doc["link"])
        j_doc["web_pages"] = self.get_web_pages(j_doc["link"])
        j_doc["similar"] = self.get_similars(j_doc["link"])

        # remove the "link" list from the "j_doc" dictionary
        del j_doc["link"]

        return j_doc


# N = NetflixRESTfulAPI("b9ycsdp9k2dzjuqkk335emsx", "qwYf6c83X7")
# r = N.search_by_id("series", "70180387")
# r = N.search_by_id("movies", "70002379")
# r = N.search_by_id("movies", "70153850")
# r = N.search_by_id("series", "70143831")
# r = N.search_by_id("series", "70153413")
# r = N.search_by_id("movies", "70002379") --> TODO: directors TypeError

# R = RottenTomatoesAPI("q5ahg5hbv7gf3nprfzkuj44y")
# r = R.search_by_title("Troy")["links"]["reviews"]
# r = R.search_by_title("Troy: Myth or Reality?")
# r = R.search_by_title("Stephen King"s Bag of Bones")
# r = R.search_by_title("Troy: For Love and War")
# r = R.search_by_title("Dias de Futbol")
# r = R.search_by_title("Bones") --> To be fixed Rotten doesn"t have tv series

# from pprint import pprint
# pprint(r)
