import re
import json

import requests

from movies import MovieServiceAPI

# ============================================================================
#                            ROTTEN TOMATOES API
# ============================================================================


class RottenTomatoesError(Exception):
    """ Error thrown if the RottenTomatoes api is throws an http error """
    pass


class RottenTomatoesAPI(MovieServiceAPI):
    """
        Class that instantiates RottenTomatoe"s API, contains methods
        to search the movies by id and by title.
    """

    def __init__(self, KEY):
        self.KEY = KEY

    # -----------------------------------------
    #              Aux functions
    # -----------------------------------------

    def _get_url_tomatoes(self, query, limit, q_type):

        base_url = "http://api.rottentomatoes.com/api/public/v1.0"

        if q_type == "title":
            search_url = "/movies.json?apikey=%s&q=%s&page_limit=%s" \
                         % (self.KEY, query, limit)
            return base_url + search_url

        if q_type == "id":
            search_url = "/movies/%s.json?apikey=%s" % (query, self.KEY)

        return base_url + search_url

    def get_movie_reviews(self, movie_url):
        """
            Finds the reviews for an specific movie, once it has been found by
            its title. It returns, reviews for the movie under the movie_url
            params:
             :param: str: movie_url: contains the url to retrieve the comments
                                     of a particular movie.
            rType: JSON
        """
        movie_url = "%s?apikey=%s" % (movie_url, self.KEY)
        r = requests.get(movie_url)
        reviews = json.loads(r.content)["reviews"]

        if isinstance(reviews, list) and len(reviews) >= 1:
            return reviews
        else:
            return None

    def get_movie_trailer(self, movie_url):
        """
            Finds a trailer for an specific movie, once it has been found by
            its title. Returns, reviews for a movie given a movie url.
            params:
             :param: str: movie_url: contains the url to retrieve the trailer
                                     of a particular movie.
            Return_Type: JSON
        """
        # TODO: Create method to get trailers
        pass

    # -----------------------------------------
    #              Search Functions
    # -----------------------------------------

    def search_by_title(self, search_term):
        """
            Returns a tuple with a list of dicts (movies) and its length
        """
        raw_data = requests.get(self._get_url_tomatoes(
            search_term, 1, "title"))
        json_doc = json.loads(raw_data.content)
        results = json_doc["movies"]

        for movie in results:
            if re.search(r".*poster_default\.gif",
                         movie["posters"]["thumbnail"]):
                del results[results.index(movie)]
        return results[0]

    def search_by_id(self, t_id):
        """
            Returns a dict with the data for the movie under t_id
        """
        raw_data = requests.get(self._get_url_tomatoes(t_id, None, "id"))
        json_doc = json.loads(raw_data.content)
        return json_doc

