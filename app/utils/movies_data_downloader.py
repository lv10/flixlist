import requests
import re
import database


url_titles = "http://odata.netflix.com/Catalog/Titles?$format=json"
title_data = requests.get(url_titles)
json_title = title_data.json()


def find_skiptoken(pattern, text):
    match = re.search(pattern, text)
    if match:
        return match.group()
    else:
        return "not found"

counter = 0


def get_titles(url):
    global counter
    title_data = requests.get(url)
    json_title = title_data.json()

    try:
        counter += 1
        print counter
        for g in json_title["d"]["results"]:
            database.insert_movie(g)

        skip_token = find_skiptoken(r"skiptoken='(.*)",
                                    json_title["d"]["__next"])
        new_url = "%s&$%s" % (url_titles, skip_token)
        get_titles(new_url)

    except KeyError:
        return "All files were parsed"

get_titles(url_titles)
