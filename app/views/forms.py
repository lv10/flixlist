from wtforms import Form, validators
from wtforms import TextField

#--------------------------------------------------
#
#                   Search Form
#
#--------------------------------------------------


class SearchForm(Form):
    search_terms = TextField("", [validators.Required(),
                             validators.Length(min=2, max=75)])
