from flask import render_template, redirect, url_for, request, Blueprint

from app import app
from forms import SearchForm
from app.movie_service.netflix import NetflixRESTfulAPI as NT_API
from app.movie_service.rottentomatoes import RottenTomatoesAPI as RT_API


search = Blueprint("search", __name__, url_prefix="/search")

# Rotten Tomatoes API, Netflix API
RT = RT_API(app.config["ROTTEN_TOMATOES_KEY"])
NT = NT_API(app.config["NETFLIX_CONSUMER_KEY"],
            app.config["NETFLIX_SHARED_SECRET"])


# ------------------------------------------------------------
#                        Search Views
# ------------------------------------------------------------

@search.route("/", methods=["POST", "GET"])
def index():
    form = SearchForm(request.form)

    if request.method == "POST" and form.validate():
        return redirect(url_for("search.search_results",
                        search_term=form.search_terms.data))

    return render_template("index.html", form=form, title="Home")


@search.route("/<search_term>", methods=["GET"])
def search_results(search_term):

    results = NT.search_by_title(str(search_term))
    lst_results, num_results = results[0], results[1]

    if num_results == 1:
        return redirect(url_for("movies_series",
                                n_id_type=lst_results[0]["id_type"],
                                n_id=lst_results[0]["id"]))
    else:
        return render_template("search_results.html",
                               lst_results=lst_results,
                               num_results=num_results,
                               search_term=search_term)


@search.route("/result/<n_id_type>-<n_id>", methods=["GET"])
def movies_series(n_id_type, n_id):

    result = NT.search_by_id(n_id_type, n_id)

    if n_id_type == "movies":
        #Get movie title when selected from Netflix
        title = result["title"]["regular"]
        res = RT.search_by_title(title)

        if res["links"]["reviews"]:
            reviews = RT.get_movie_reviews(res["links"]["reviews"])
            return render_template("result.html", result=result,
                                   reviews=reviews)
        else:
            return render_template("result.html", result=result, reviews=None)

    return render_template("result.html", result=result, reviews=None)
